﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Religion 
{
    Put,
    Kid
}

public enum State
{
    Move,
    MoveToward,
    SpawnBat
}

public class GhostBase : MonoBehaviour
{
    [SerializeField]
    protected Religion religion;

    [SerializeField]
    protected int health = 2;

    [SerializeField]
    protected float moveSpeed = 1.0f;

    [SerializeField]
    protected GameObject boom;

    Direction moveDirection = Direction.Idle;

    public State currentState = State.Move;
    public void SetMoveDirection(Direction moveDirection) { this.moveDirection = moveDirection; }

    float countToSeek = 2.0f;

    float currentCountToSeek;

    void Start()
    {
        OnStart();
    }

    public virtual void OnStart() 
    {
        currentCountToSeek = countToSeek;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        OnUpdate();
    }

    public virtual void OnUpdate()
    {
        currentCountToSeek -= Time.deltaTime;

        if (currentCountToSeek <= 0) 
        {
            currentState = State.MoveToward;
        }


        if (currentState == State.Move)
        {
            if (moveDirection == Direction.Right)
            {
                this.transform.position += new Vector3(moveSpeed * Time.deltaTime, 0.0f, 0.0f);
            }
            else if (moveDirection == Direction.Left)
            {
                this.transform.position -= new Vector3(moveSpeed * Time.deltaTime, 0.0f, 0.0f);
            }
            else if (moveDirection == Direction.Up)
            {
                this.transform.position += new Vector3(0.0f, moveSpeed * Time.deltaTime, 0.0f);
            }
            else if (moveDirection == Direction.Down)
            {
                this.transform.position -= new Vector3(0.0f, moveSpeed * Time.deltaTime, 0.0f);
            }
        } 
        else if (currentState == State.MoveToward) 
        {
            SeekTarget(TempleAndChurch.Instance.transform.position);
        }

    }

    public void SeekTarget(Vector3 targetPosition)
    {
        Vector3 position = transform.position;
        Vector3 targetDirection = targetPosition - transform.position;
        Vector3 desireVelocity = Vector3.Normalize(targetDirection) * moveSpeed * 2.0f;

        position += desireVelocity * Time.deltaTime;
        transform.position = position;
    }


    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Bullet>()) 
        {
            if (religion == collision.GetComponent<Bullet>().GetReligion()) 
            {
                health--;
                if (health <= 0) 
                {
                    Destroy(this.gameObject);
                    Instantiate(boom, this.transform.position, this.transform.rotation);
                }
            }
        }
    }
}
