﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempleAndChurch : MonoBehaviour
{
    private static TempleAndChurch _instance;

    public static TempleAndChurch Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<TempleAndChurch>();
            }

            return _instance;
        }
    }

    [SerializeField]
    float maxHealth = 10;

    [SerializeField]
    Slider healthUI;

    [SerializeField]
    Animator anim;

    float currentHealth;



    private void Start()
    {
        currentHealth = maxHealth;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<GhostBase>())
        {
            Destroy(collision.gameObject);
            anim.SetTrigger("Damaged");
            currentHealth--;
            healthUI.value =  (currentHealth / maxHealth);
            if (currentHealth <= 0)
            {
                GameManager.Instance.GameOver();
            }
        }
    }
}
