﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostSpawner : MonoBehaviour
{
    public List<SpawnPoint> spawnPoints = new List<SpawnPoint>();
    public List<GhostBase> ghostSpawnList = new List<GhostBase>();

    public int startSpawnTime = 1;
    public float spawnTime = 1;

    void Start()
    {
        InvokeRepeating("Spawn", startSpawnTime, spawnTime);
    }

    void Spawn()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPoint = Random.Range(0, spawnPoints.Count);
        int randomGhost = Random.Range(0, ghostSpawnList.Count);

        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        GhostBase ghost = Instantiate(ghostSpawnList[randomGhost], this.spawnPoints[spawnPoint].transform.position, this.spawnPoints[spawnPoint].transform.rotation);

        ghost.SetMoveDirection(spawnPoints[spawnPoint].GetDirection());
    }

    public void StartInvokeReapeating() 
    {
        InvokeRepeating("Spawn", startSpawnTime, spawnTime);
    }
}
