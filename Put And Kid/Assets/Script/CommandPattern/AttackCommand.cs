﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AttackCommand", menuName = "ScriptableObjects/AttackCommand", order = 2)]
public class AttackCommand : Command
{
    public enum AttackDirection
    {
        Up,
        Down,
        Left,
        Right
    }

    [SerializeField]
    private AttackDirection attackDirection;

    public override void Execute(GameActor actor)
    {
        actor.GetAnimator().SetTrigger(attackDirection.ToString());
        Debug.Log("Attack");
    }
}
