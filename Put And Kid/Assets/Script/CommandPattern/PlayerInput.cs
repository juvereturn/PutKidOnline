﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    private GameActor actor;
    
    private InputHandler inputHandler;

    private Command currentCommand;

    private void Start()
    {
        inputHandler = GetComponent<InputHandler>();
    }

    private void Update()
    {
        currentCommand = inputHandler.HandleInput();
        if (currentCommand)
        {
            currentCommand.Execute(actor);
        }
    }

    void FixedUpdate()
    {


    }
}
