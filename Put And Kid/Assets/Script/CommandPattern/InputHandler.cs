﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    #region Private Variable

    [SerializeField]
    private KeyCode AttackRight;

    [SerializeField]
    private KeyCode AttackLeft;

    [SerializeField]
    private KeyCode AttackUp;

    [SerializeField]
    private KeyCode AttackDown;

    [SerializeField]
    private KeyCode MoveRight;

    [SerializeField]
    private KeyCode MoveLeft;

    [SerializeField]
    private KeyCode MoveUp;

    [SerializeField]
    private KeyCode MoveDown;

    [SerializeField]
    private Command buttonX_;
    [SerializeField]
    private Command buttonY_;
    [SerializeField]
    private Command buttonA_;
    [SerializeField]
    private Command buttonB_;
    [SerializeField]
    private Command buttonLeft_;
    [SerializeField]
    private Command buttonUp_;
    [SerializeField]
    private Command buttonDown_;
    [SerializeField]
    private Command buttonRight_;

    #endregion

    public Command HandleInput()
    {
        if (Input.GetKey(MoveLeft)) return buttonLeft_;
        if (Input.GetKey(MoveUp)) return buttonUp_;
        if (Input.GetKey(MoveDown)) return buttonDown_;
        if (Input.GetKey(MoveRight)) return buttonRight_;
        if (Input.GetKeyDown(AttackLeft)) return buttonX_;
        if (Input.GetKeyDown(AttackUp)) return buttonY_;
        if (Input.GetKeyDown(AttackDown)) return buttonA_;
        if (Input.GetKeyDown(AttackRight)) return buttonB_;
        // Nothing pressed, so do nothing.
        return null;
    }
}
