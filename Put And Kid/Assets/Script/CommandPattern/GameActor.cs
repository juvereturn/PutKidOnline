﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameActor : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private Animator animator;

    public float GetMoveSpeed() { return moveSpeed; }

    public Animator GetAnimator() { return animator; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
