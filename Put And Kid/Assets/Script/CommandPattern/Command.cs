﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Command : ScriptableObject
{
    public virtual void Execute(GameActor actor) 
    {
        
    }
}
