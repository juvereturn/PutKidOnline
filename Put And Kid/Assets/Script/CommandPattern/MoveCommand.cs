﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveCommand", menuName = "ScriptableObjects/MoveCommand", order = 1)]
public class MoveCommand : Command
{
    [SerializeField]
    protected Vector3 moveDirection;

    public override void Execute(GameActor actor)
    {
        actor.transform.position += (actor.GetMoveSpeed() * Time.deltaTime) * moveDirection;
    }
}
