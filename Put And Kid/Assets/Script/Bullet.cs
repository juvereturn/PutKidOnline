﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction 
{
    Idle,
    Left,
    Right,
    Up,
    Down
}

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float bulletSpeed = 1.0f;

    [SerializeField]
    Religion religion;

    Direction shootDirection = Direction.Idle;

    float countDownToDestroyItselfTime = 5f;

    float currentCountDownToDestroyItselfTime;

    public void SetShootDirection(Direction shootDirection) { this.shootDirection = shootDirection; }

    public Religion GetReligion() { return religion; }

    private void Start()
    {
        currentCountDownToDestroyItselfTime = countDownToDestroyItselfTime;
    }

    private void Update()
    {
        currentCountDownToDestroyItselfTime -= Time.deltaTime;

        if (shootDirection == Direction.Right)
        {
            this.transform.position += new Vector3(bulletSpeed * Time.deltaTime, 0.0f, 0.0f);
        } 
        else if (shootDirection == Direction.Left) 
        {
            this.transform.position -= new Vector3(bulletSpeed * Time.deltaTime, 0.0f, 0.0f);
        }
        else if(shootDirection == Direction.Up)
        {
            this.transform.position += new Vector3(0.0f, bulletSpeed * Time.deltaTime, 0.0f);
        }

        else if (shootDirection == Direction.Down)
        {
            this.transform.position -= new Vector3(0.0f, bulletSpeed * Time.deltaTime, 0.0f);
        }
        if (currentCountDownToDestroyItselfTime <= 0) 
        {
            Destroy(this.gameObject);
        }

    }
}
