﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{

    public void GoToTutorial()
    {
        SceneManager.LoadScene("TutorialScene");
    }

    public void GoToOnlineLobby()
    {
        SceneManager.LoadScene("MultiplayerLobby");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
