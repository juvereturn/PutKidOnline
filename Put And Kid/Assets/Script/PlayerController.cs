﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    Animator animator;

    [SerializeField]
    KeyCode MoveRight;

    [SerializeField]
    KeyCode MoveLeft;

    [SerializeField]
    KeyCode MoveUp;

    [SerializeField]
    KeyCode MoveDown;

    [SerializeField]
    KeyCode AttackRight;

    [SerializeField]
    KeyCode AttackLeft;

    [SerializeField]
    KeyCode AttackUp;

    [SerializeField]
    KeyCode AttackDown;

    [SerializeField]
    float moveSpeed;

    [SerializeField]
    GameObject[] heart;

    int hp = 3;

    [SerializeField]
    GameObject deathGameObj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(MoveRight)) 
        {
            this.transform.position += new Vector3(moveSpeed * Time.deltaTime, 0.0f, 0.0f);
        }

        if (Input.GetKey(MoveLeft))
        {
            this.transform.position -= new Vector3(moveSpeed * Time.deltaTime, 0.0f, 0.0f);
        }

        if (Input.GetKey(MoveUp))
        {
            this.transform.position += new Vector3(0.0f, moveSpeed * Time.deltaTime, 0.0f);
        }

        if (Input.GetKey(MoveDown))
        {
            this.transform.position -= new Vector3(0.0f, moveSpeed * Time.deltaTime, 0.0f);
        }

        if (Input.GetKeyDown(AttackRight)) 
        {
            animator.SetTrigger("Right");
        }
        if (Input.GetKeyDown(AttackLeft))
        {
            animator.SetTrigger("Left");
        }
        if (Input.GetKeyDown(AttackUp))
        {
            animator.SetTrigger("Up");
        }
        if (Input.GetKeyDown(AttackDown))
        {
            animator.SetTrigger("Down");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<GhostBase>())
        {
            Destroy(collision.gameObject);
            GetComponent<Animator>().SetTrigger("Damaged");
            hp--;
            heart[hp].SetActive(false);
            if (hp <= 0)
            {
                Instantiate(deathGameObj, this.transform.position, this.transform.rotation);
                Destroy(this.gameObject);
            }
        }
    }
}
