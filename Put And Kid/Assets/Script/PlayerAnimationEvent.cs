﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvent : MonoBehaviour
{
    [SerializeField]
    Bullet bullet;

    [SerializeField]
    Transform leftGun;

    [SerializeField]
    Transform rightGun;

    [SerializeField]
    Transform upGun;

    [SerializeField]
    Transform downGun;

    [SerializeField]
    float cooldownTime = 1.0f;

    float currentCooldown = 0;

    private void Update()
    {
        if (currentCooldown >= 0) 
        {
            currentCooldown -= Time.deltaTime;
        }
    }

    public void ShootRight() 
    {
        SpawnBullet(rightGun, Direction.Right);
    }

    public void ShootLeft()
    {
        SpawnBullet(leftGun, Direction.Left);
    }

    public void ShootUp()
    {
        SpawnBullet(upGun, Direction.Up);
    }

    public void ShootDown()
    {
        SpawnBullet(downGun, Direction.Down);
    }

    void SpawnBullet(Transform gun, Direction dir) 
    {
        if (currentCooldown <= 0) 
        {
            GameObject bulletTemp = Instantiate(bullet.gameObject, gun.transform.position, bullet.transform.rotation);
            bulletTemp.GetComponent<Bullet>().SetShootDirection(dir);
            currentCooldown = cooldownTime;
        }

    }

}
