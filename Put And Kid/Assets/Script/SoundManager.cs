﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    AudioMixerGroup sfxAudiogroup;

    public SoundClass[] Sounds;

    [SerializeField]
    float masterVolume = 1.0f;

    #region Singleton

    private static SoundManager _instance;

    public static SoundManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        foreach (SoundClass s in Sounds)
        {
            if (s.clip != null)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.outputAudioMixerGroup = sfxAudiogroup;
                s.source.clip = s.clip;
                s.source.volume = s.vol;
                s.source.pitch = s.pitch;
                s.source.loop = s.Loop;
                s.source.playOnAwake = s.PlayOnAwake;
                s.source.spatialBlend = s.spatialBlend;
            }
        }
    }

    #endregion

    private void Start()
    {
        AudioListener.volume = masterVolume;
    }

    public void ChangePitch(string name, float pitch)
    {
        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        pitch = Mathf.Clamp(pitch, 0.1f, 3.0f);
        s.source.pitch = pitch;
    }

    public void ChangeVolume(string name, float vol)
    {
        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        vol = Mathf.Clamp(vol, 0f, 1.0f);
        s.source.volume = vol;
    }

    public void EnableLoop(string name)
    {

        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        s.source.loop = true;
    }

    public void StopLoop(string name)
    {

        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        s.source.loop = false;
    }

    public void Play(string name)
    {
        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        s.source.Play();
    }

    public void PlayWhileOtherSoundIsNotPlaying(string name)
    {

        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        if (s.source.isPlaying == false)
        {
            s.source.Play();
        }

    }

    public void AssignAudioSource(AudioSource source, string audioName)
    {
        foreach (SoundClass s in Sounds)
        {
            if (s.name == audioName)
            {
                source.clip = s.clip;
                source.volume = s.vol;
                source.pitch = s.pitch;
                source.loop = s.Loop;
                source.playOnAwake = s.PlayOnAwake;
                source.spatialBlend = s.spatialBlend;
            }
        }

    }

    public void AssignAudioSourceAndPlayLoop(AudioSource source, string audioName)
    {
        foreach (SoundClass s in Sounds)
        {
            if (s.name == audioName)
            {
                source.clip = s.clip;
                source.volume = s.vol;
                source.pitch = s.pitch;
                source.loop = s.Loop;
                source.playOnAwake = s.PlayOnAwake;
                source.spatialBlend = s.spatialBlend;
            }
        }
        source.loop = true;
        if (!source.isPlaying)
            source.Play();
    }

    public void AssignAudioSourceAndPlayOneShot(AudioSource source, string audioName)
    {
        foreach (SoundClass s in Sounds)
        {
            if (s.name == audioName)
            {
                source.clip = s.clip;
                source.volume = s.vol;
                source.pitch = s.pitch;
                source.loop = s.Loop;
                source.playOnAwake = s.PlayOnAwake;
                source.spatialBlend = s.spatialBlend;
            }
        }
        source.loop = false;
        source.PlayOneShot(source.clip);
    }

    public void Stop(string name)
    {
        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        s.source.Stop();
        s.source.loop = false;
    }

    public void PlayOneShot(string name)
    {
        SoundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        Debug.Log("Sound: " + s.clip.name + "Found");
        s.source.PlayOneShot(s.source.clip);
    }

    public string RandomSound(string[] audioNameList)
    {
        if (audioNameList.Length <= 0)
        {
            return "";
        }

        return audioNameList[UnityEngine.Random.Range(0, audioNameList.Length)];
    }

}