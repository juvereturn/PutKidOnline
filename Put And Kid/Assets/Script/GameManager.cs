﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Phase 
{
    public List<GhostBase> ghostSpawnList = new List<GhostBase>();

    public float spawnTime = 1;

    public float coolDownTime = 30.0f;
} 


public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    int currentPhase = 0;

    float countDownToAnotherPhaseTime = 5.0f;

    float currentCountDown;

    bool isCountingDown = false;

    [SerializeField]
    Phase[] phases;

    [SerializeField]
    GhostSpawner ghostSpawner;

    [SerializeField]
    GameObject youWInUI;

    public void GameOver() 
    {
        SceneManager.LoadScene("LoseScene");
    }

    private void Start()
    {
        currentCountDown = countDownToAnotherPhaseTime;
        isCountingDown = true;
    }

    private void FixedUpdate()
    {
        if (isCountingDown) 
        {
           
            currentCountDown -= Time.deltaTime;
            if (currentCountDown <= 0) 
            {
                isCountingDown = false;
                GoToNextPhase();
            }
        }
        
    }

    void GoToNextPhase() 
    {
        currentPhase++;
        if (currentPhase >= phases.Length) 
        {
            currentPhase = 0;

            ghostSpawner.CancelInvoke();

            GhostBase[] allGhost = FindObjectsOfType<GhostBase>();

            for (int i = 0; i < allGhost.Length; i++) 
            {
                Destroy(allGhost[i].gameObject);
            }

            youWInUI.SetActive(true);

            SoundManager.Instance.PlayOneShot("Tada");

            StartCoroutine(WaitAndLoad(5));

            return;
            
            
        }
        ghostSpawner.CancelInvoke();
        ghostSpawner.ghostSpawnList = phases[currentPhase].ghostSpawnList;

        ghostSpawner.spawnTime = phases[currentPhase].spawnTime;

        ghostSpawner.StartInvokeReapeating();
        isCountingDown = true;
        currentCountDown = phases[currentPhase].coolDownTime;
    }

    private IEnumerator WaitAndLoad(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("WinScene");
    }

}
