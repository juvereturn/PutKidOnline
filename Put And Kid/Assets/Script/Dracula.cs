﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dracula : GhostBase
{
    [SerializeField]
    GameObject bat;


    [SerializeField]
    float cooldownTime = 1.0f;

    float currentCooldown = 0;

    public override void OnStart()
    {

    }

    public override void OnUpdate() 
    {
        if (currentCooldown >= 0)
        {
            currentCooldown -= Time.deltaTime;
        }
    }

    public void SpawnBat() 
    {
        if (currentCooldown <= 0)
        {
            GameObject batTemp = Instantiate(bat, this.transform.position, this.transform.rotation);
            batTemp.GetComponent<GhostBase>().currentState = State.MoveToward;
            currentCooldown = cooldownTime;
        }
        

    }
}
