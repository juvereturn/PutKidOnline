﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KrasueBat : GhostBase
{
    [SerializeField]
    GameObject bat;

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Bullet>())
        {
            if (collision.GetComponent<Bullet>().GetReligion() == Religion.Put && health >= 2)
            {
                GameObject batTemp = Instantiate(bat, this.transform.position, this.transform.rotation);
                batTemp.GetComponent<GhostBase>().currentState = State.MoveToward;
                Destroy(this.gameObject);
            }
            if (religion == collision.GetComponent<Bullet>().GetReligion())
            {
                health--;
                religion = Religion.Put;
                GetComponent<Animator>().SetBool("Hurt", true);
                if (health <= 0)
                {
                    Destroy(this.gameObject);
                    Instantiate(boom, this.transform.position, this.transform.rotation);
                }
                
            }
        }
    }
}
