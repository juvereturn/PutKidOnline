﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Next : MonoBehaviour
{
    [SerializeField]
    GameObject currentGameObj;

    [SerializeField]
    GameObject nextGameObj;

    public void GoNext() 
    {
        if (nextGameObj == null) 
        {
            SceneManager.LoadScene("GameScene");
            return;
        }

        nextGameObj.SetActive(true);
        currentGameObj.SetActive(false);
    }
}
