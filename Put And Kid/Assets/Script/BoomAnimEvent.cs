﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomAnimEvent : MonoBehaviour
{
    public void OnBoomEnd() 
    {
        Destroy(this.gameObject);
    }
}
